//
//  AddGeofenceViewController.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import MapKit

protocol AddGeofenceViewControllerDelegate {
    func addGeofenceViewControllerViewController(controller: AddGeofenceViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                                 radius: Double, identifier: String, note: String, eventType: EventType)
}

class AddGeofenceViewController: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet var addButton: UIBarButtonItem!
    @IBOutlet var zoomButton: UIBarButtonItem!
    @IBOutlet weak var eventTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    var delegate: AddGeofenceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItems = [addButton, zoomButton]
        addButton.isEnabled = false
        
        radiusTextField.delegate = self
        noteTextField.delegate = self
        
        self.tableView.tableFooterView = UIView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    @IBAction func textFieldEditingChanged(sender: UITextField) {
        addButton.isEnabled = !radiusTextField.text!.isEmpty && !noteTextField.text!.isEmpty
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        dismiss(animated: true, completion: nil)
//        showFirstViewController()
    }
    
    @IBAction private func onAdd(sender: AnyObject) {
        let coordinate = mapView.centerCoordinate
        let radius = Double(radiusTextField.text!) ?? 0
        let identifier = NSUUID().uuidString
        let note = noteTextField.text
        let eventType: EventType = (eventTypeSegmentedControl.selectedSegmentIndex == 0) ? .onEntry : .onExit
        delegate?.addGeofenceViewControllerViewController(controller: self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, note: note!, eventType: eventType)
    }
    
    @IBAction private func onZoomToCurrentLocation(sender: AnyObject) {
        mapView.zoomToUserLocation()
    }
    
    func showFirstViewController() {
        self.performSegue(withIdentifier: "idHomeSegueUnwind", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            radiusTextField.becomeFirstResponder()
        }
        else if indexPath.row == 2 {
            noteTextField.becomeFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
    }
    
}
