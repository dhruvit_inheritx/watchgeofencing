//
//  GeofenceListCell.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class GeofenceListCell: UITableViewCell {

    @IBOutlet var lblLatitude : UILabel!
    @IBOutlet var lblLongitude : UILabel!
    @IBOutlet var lblRadius : UILabel!
    @IBOutlet var lblIdentifier : UILabel!
    @IBOutlet var lblNote : UILabel!
    @IBOutlet var lblEventType : UILabel!
    
    func configureCell(geofence : Geofence) {
        self.lblIdentifier.text = geofence.identifier
        self.lblLatitude.text = "\(geofence.coordinate.latitude)"
        self.lblLongitude.text = "\(geofence.coordinate.longitude)"
        self.lblRadius.text = "\(geofence.radius)"
        self.lblNote.text = geofence.note
        self.lblEventType.text = geofence.eventType.rawValue
    }
    
    override func prepareForReuse() {
        self.lblIdentifier.text = ""
        self.lblLatitude.text = ""
        self.lblLongitude.text = ""
        self.lblRadius.text = ""
        self.lblNote.text = ""
        self.lblEventType.text = ""
    }

}
