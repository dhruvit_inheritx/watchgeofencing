//
//  AppDelegate.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import CoreLocation
import WatchConnectivity

@available(iOS 9.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var watchSession : WCSession!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.requestLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        if WCSession.isSupported() {
            watchSession = WCSession.default()
            watchSession.delegate = self
            watchSession.activate()
        }
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        UIApplication.shared.cancelAllLocalNotifications()

        return true
    }

    func handleEvent(forRegion region: CLRegion!) {
        // Show an alert if application is active
        if UIApplication.shared.applicationState == .active {
            guard let message = note(fromRegionIdentifier: region.identifier) else {
                return
            }
            window?.rootViewController?.showAlert(withTitle: nil, message: message)
        } else {
            
            guard let message = note(fromRegionIdentifier: region.identifier) else {
                return
            }
            
            // Otherwise present a local notification
//            let notification = UILocalNotification()
//            notification.alertBody = message
//            notification.soundName = "Default"
//            UIApplication.shared.presentLocalNotificationNow(notification)
            
            sendMesaageToWatch(strNote: message)
        }
    }
    
    func note(fromRegionIdentifier identifier:String) -> String? {
        let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) as? [Data]
        let geofences = savedItems?.map { NSKeyedUnarchiver.unarchiveObject(with: $0 as Data) as? Geofence }
        let index = geofences?.index { $0?.identifier == identifier }
        return index != nil ? geofences?[index!]?.note : nil
    }

    func sendMesaageToWatch(strNote : String) {
        
        let dictData = [kNote:strNote]
        watchSession.sendMessage(["data":dictData], replyHandler: { (response) in
            print("Response :- \(response)")
        }) { (error) in
            print("error :- \(error)")
        }
    }
    
}

@available(iOS 9.0, *)
extension AppDelegate : CLLocationManagerDelegate {
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        print("didUpdateLocations")
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("didFailWithError")
//    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
}

@available(iOS 9.3, *)
extension AppDelegate : WCSessionDelegate {
    
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("activationDidCompleteWith")
    }
    
    public func sessionDidBecomeInactive(_ session: WCSession) {
        print("sessionDidBecomeInactive")
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
        print("sessionDidDeactivate")
    }
    
}
