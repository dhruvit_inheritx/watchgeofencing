//
//  ViewController.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

struct PreferencesKeys {
    static let savedItems = "savedItems"
    static let CellIDGeofenceList = "geofenceListCell"
}

class ViewController: UIViewController {
    
    @IBOutlet var mapView : MKMapView!
    var geofences : [Geofence] = []
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        loadAllGeoFences()
        
        var points=[CLLocationCoordinate2DMake(33.8031776,-117.4628518), CLLocationCoordinate2DMake(33.8351523,-117.9861854), CLLocationCoordinate2DMake(33.6869803,-117.8442924), CLLocationCoordinate2DMake(33.6884737,-117.3878109), CLLocationCoordinate2DMake(33.7919732,-117.2912004), CLLocationCoordinate2DMake(34.0201812,-118.6919246)]
        
        let polygon = MKPolygon(coordinates: &points, count: points.count)
        mapView.add(polygon)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addGeofence" {
            let navigationController = segue.destination as! UINavigationController
            let vc = navigationController.viewControllers.first as! AddGeofenceViewController
            vc.delegate = self
        }
        if segue.identifier == "listGeofence" {
            let navigationController = segue.destination as! UINavigationController
            let vc = navigationController.viewControllers.first as! GeofenceListTableViewController
            vc.delegate = self
        }
    }
    
    func loadAllGeoFences() {
        geofences = []
        
        guard let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) else  {
            return
        }
        
        for savedItem in savedItems {
            guard let geofence = NSKeyedUnarchiver.unarchiveObject(with: savedItem as! Data) as? Geofence else { continue }
            add(geofence: geofence)
        }
    }
    
    func saveAllGeofences() {
        var items : [Data] = []
        for geofences in geofences{
            let item = NSKeyedArchiver.archivedData(withRootObject: geofences)
            items.append(item)
        }
        
        UserDefaults.standard.set(items, forKey: PreferencesKeys.savedItems)
    }
    
    func add(geofence : Geofence) {
        geofences.append(geofence)
        mapView.addAnnotation(geofence)
        addRadiusOverlay(forGeofence: geofence)
        updateGeofencesCount()
    }
    
    func remove(geofence : Geofence) {
        if let indexInArray = geofences.index(of: geofence) {
            geofences.remove(at: indexInArray)
        }
        mapView.removeAnnotation(geofence)
        removeRadiusOverlay(forGeotification: geofence)
        updateGeofencesCount()
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeofence geofence : Geofence) {
        mapView.add(MKCircle(center: geofence.coordinate, radius: geofence.radius))
    }
    
    func removeRadiusOverlay(forGeotification geotification: Geofence) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                mapView?.remove(circleOverlay)
                break
            }
        }
    }
    
    func updateGeofencesCount() {
        title = "Geofences (\(geofences.count))"
        navigationItem.rightBarButtonItem?.isEnabled = (geofences.count < 20)
    }
    
    // MARK: Other mapview functions
    @IBAction func zoomToCurrentLocation(sender: AnyObject) {
        mapView.zoomToUserLocation()
    }
    
    func region(withGeofence geofence : Geofence ) -> CLCircularRegion {
        
        let region = CLCircularRegion(center: geofence.coordinate, radius: geofence.radius, identifier: geofence.identifier)
        region.notifyOnEntry = (geofence.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        
        return region
    }
    
    func startMonitoring(geofence : Geofence) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle: "Error", message: "Geofencing is not supported on this device!")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlert(withTitle: "Warning", message: "Your geofence is saved but will only be activated once you grant Geo-Fence permission to access the device location.")
        }
        
        let region = self.region(withGeofence: geofence)
        locationManager.startMonitoring(for: region)
    }
    
    func stopMonitoring(geofence : Geofence) {
        
        for region in locationManager.monitoredRegions {
            
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geofence.identifier else { continue }
            
            locationManager.stopMonitoring(for: circularRegion)
        }
        
    }
    
}

// MARK: AddGeotificationViewControllerDelegate
extension ViewController: AddGeofenceViewControllerDelegate {
    
    func addGeofenceViewControllerViewController(controller: AddGeofenceViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {
        controller.dismiss(animated: true, completion: nil)
        // 1
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geofence = Geofence(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType)
        add(geofence: geofence)
        // 2
        startMonitoring(geofence:  geofence)
        saveAllGeofences()
    }
    
}

extension ViewController: GeofenceListTableViewControllerDelegate {
    
    func geofenceListTableViewController(controller: GeofenceListTableViewController) {
        
        controller.dismiss(animated: true, completion: nil)
        
        mapView.removeOverlays(mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        loadAllGeoFences()
        updateGeofencesCount()
    }
    
}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = status == .authorizedAlways
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
}

extension ViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeofence"
        if annotation is Geofence {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(#imageLiteral(resourceName: "DeleteGeofence"), for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        //        if overlay is MKPolygon {
        //            let polygonView = MKPolygonRenderer(overlay: overlay)
        //            polygonView.lineWidth = 1.0
        //            polygonView.strokeColor = .purple
        //            polygonView.fillColor = UIColor.purple.withAlphaComponent(0.4)
        //
        //            return polygonView
        //        }
        
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        
        return  MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geofence = view.annotation as! Geofence
        remove(geofence: geofence)
        saveAllGeofences()
    }
    
}
