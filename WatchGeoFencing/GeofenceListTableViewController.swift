//
//  GeofenceListTableViewController.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import MapKit

protocol GeofenceListTableViewControllerDelegate {
    func geofenceListTableViewController(controller: GeofenceListTableViewController)
}

class GeofenceListTableViewController: UITableViewController {

    var geofences : [Geofence] = []

    var delegate: GeofenceListTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        loadAllGeoFences()
    }

    func loadAllGeoFences() {
        geofences = []
        
        guard let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) else  {
            return
        }
        
        for savedItem in savedItems {
            guard let geofence = NSKeyedUnarchiver.unarchiveObject(with: savedItem as! Data) as? Geofence else { continue }
            geofences.append(geofence)
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        delegate?.geofenceListTableViewController(controller: self)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return geofences.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PreferencesKeys.CellIDGeofenceList, for: indexPath) as! GeofenceListCell

        // Configure the cell...

        cell.configureCell(geofence: geofences[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 187
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            // Delete the row from the data source
            remove(geofence: geofences[indexPath.row])
        }
    }

    func remove(geofence : Geofence) {
        if let indexInArray = geofences.index(of: geofence) {
            geofences.remove(at: indexInArray)
        }
        saveAllGeofences()
        loadAllGeoFences()
    }
    
    func saveAllGeofences() {
        var items : [Data] = []
        for geofences in geofences {
            let item = NSKeyedArchiver.archivedData(withRootObject: geofences)
            items.append(item)
        }
        
        UserDefaults.standard.set(items, forKey: PreferencesKeys.savedItems)
    }
    
}
