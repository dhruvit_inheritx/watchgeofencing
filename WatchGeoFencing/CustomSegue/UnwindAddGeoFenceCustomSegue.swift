//
//  UnwindAddGeoFenceCustomSegue.swift
//  WatchGeoFencing
//
//  Created by Dhruvit on 17/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit

class UnwindAddGeoFenceCustomSegue: UIStoryboardSegue {

    override func perform() {
        // Assign the source and destination views to local variables.
        let secondVCView = self.source.view as UIView!
        let firstVCView = self.destination.view as UIView!
        
        let screenWidth = UIScreen.main.bounds.size.width
        //        let screenHeight = UIScreen.mainScreen().bounds.size.height
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(firstVCView!, aboveSubview: secondVCView!)
        
        
        // Animate the transition.
        UIView.animate(withDuration: 0.4, animations: {
            
            firstVCView?.frame = (firstVCView?.frame.offsetBy(dx: screenWidth, dy: 0.0))!
            secondVCView?.frame = (secondVCView?.frame.offsetBy(dx: screenWidth, dy: 0.0))!
            
        }) { (finished) -> Void in            
            self.source.dismiss(animated: false, completion: nil)
        }
    }
}
