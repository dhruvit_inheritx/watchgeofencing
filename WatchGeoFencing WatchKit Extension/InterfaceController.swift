//
//  InterfaceController.swift
//  WatchGeoFencing WatchKit Extension
//
//  Created by Dhruvit on 10/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation
import CoreLocation
import WatchConnectivity

class InterfaceController: WKInterfaceController {

    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var currentRegion :MKCoordinateRegion
    var currentSpan :MKCoordinateSpan
    
    @IBOutlet var map : WKInterfaceMap!
    
    override init() {
        let coordinate = CLLocationCoordinate2D(
            latitude: 51.505248,
            longitude: -0.113838)
        
        currentSpan = MKCoordinateSpanMake(0.005, 0.005)
        currentRegion = MKCoordinateRegionForMapRect(MKMapRectMake(0, 0, 1.0, 1.0))
        currentRegion = MKCoordinateRegionMake(coordinate, currentSpan)
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        map.setRegion(currentRegion)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func zoomIn() {
        let span = MKCoordinateSpan(latitudeDelta: currentSpan.latitudeDelta * 0.5, longitudeDelta: currentSpan.longitudeDelta * 0.5)
        let region = MKCoordinateRegion(center: currentLocation.coordinate, span: span)
        currentSpan = span
        map.setRegion(region)
    }
    
    @IBAction func zoomOut() {
        let span = MKCoordinateSpan(latitudeDelta: currentSpan.latitudeDelta * 2, longitudeDelta: currentSpan.longitudeDelta * 2)
        let region = MKCoordinateRegion(center: currentLocation.coordinate, span: span)
        currentSpan = span
        map.setRegion(region)
    }
    
    @IBAction func showUserLocation() {
        let coordinate = currentLocation.coordinate
        setMapTo(coordinate: coordinate)
    }
    
    func setMapTo(coordinate :CLLocationCoordinate2D) {
        let region = MKCoordinateRegionMake(coordinate, currentSpan)
        currentRegion = region;
        
        let newCenterPoint = MKMapPointForCoordinate(coordinate)
        
        map.setVisibleMapRect(MKMapRectMake(newCenterPoint.x, newCenterPoint.y, currentSpan.latitudeDelta, currentSpan.longitudeDelta))
        map.setRegion(region)
    }
}

extension InterfaceController : CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count  == 0 {
            return
        }
        
        self.currentLocation = locations.first
        
//        print("Latitude :- \(self.currentLocation.coordinate.latitude)")
//        print("Longitude :- \(self.currentLocation.coordinate.longitude)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailWithError : - \(error.localizedDescription)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorization")
    }
    
}

extension InterfaceController : WCSessionDelegate {
    
    @available(watchOSApplicationExtension 2.2, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("activationDidCompleteWith")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        print(message)
        
        let dictData = message["data"] as! [String:Any]
        print("\nNote :- \(dictData[kNote]!)")
        let strNote = dictData[kNote]
        Helper.sendLocalNotification(withTitle: strNote as! String, timeInterval: 1.0)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("session received message :- \(message)")
    }
    
}
