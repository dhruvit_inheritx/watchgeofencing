//
//  Helper.swift
//  HealthKitDemo
//
//  Created by Dhruvit on 04/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import UserNotifications

typealias alertComplitionBlock = (NSInteger) -> ()

let kNote = "note"

class Helper: NSObject {
    
    static func showAlert(withMessage alertMessage : String , controller : WKInterfaceController) {
        let actionOk = WKAlertAction(title: "Ok", style: WKAlertActionStyle.default) {}
        
        controller.presentAlert(withTitle: "WatchGeoFencing", message: alertMessage , preferredStyle: WKAlertControllerStyle.alert, actions: [actionOk])
    }
    
    static func sendLocalNotification(withTitle title : String, timeInterval : TimeInterval) {
        
        if #available(watchOSApplicationExtension 3.0, *) {
            let center = UNUserNotificationCenter.current()
            let id = String(Date().timeIntervalSinceReferenceDate)
            
            center.requestAuthorization(options: [.alert,.badge,.sound]) { (granted, error) in
                
                if granted {
                    print("granted")
                }
                else {
                    print("not granted")
                }
                
                if (error != nil) {
                    print("error is :- \(error)")
                }
            }
            
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = "Hello_message_body"
            content.sound = UNNotificationSound.default()
            content.categoryIdentifier = "REMINDER_CATEGORY"
            
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest.init(identifier: id , content: content, trigger: trigger)
            
            center.add(request, withCompletionHandler: nil)
        }
        else {
            // Fallback on earlier versions
        }
    }
}
